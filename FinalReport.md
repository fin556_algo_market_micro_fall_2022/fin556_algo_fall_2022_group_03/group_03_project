# An LSTM Stock Price Predictor trained on IEX Data, implemented in Strategy Studio

&nbsp;

## Team

### Ayan Bhowmick (Team Leader)

Ayan Bhowmick is currently a Senior at the University of Illinois at Urbana-Champaign. Following a double major in Computer Science + Economics and Statistics, Ayan has applied and is planning to continue his education doing a Masters in Computer Science, AI/ML. He has not yet decided where he will attend his Masters.

### Jimmy Huang

<img src="./images/Jimmy_Huang.jpg" width="15%" height="15%">

Jimmy Huang is a current Junior at the University of Illinois at Urbana-Champaign (UIUC). He is pursuing a double major in Mathematics + Computer Science and Statistics. Jimmy is graduating in May 2024. Next semester (Spring 2023), Jimmy will be interning at JPMorgan Chase in Chicago as part of UIUC's City Scholars program. In this internship, Jimmy will work on the Digital Communications team and utilize machine learning techniques to help the company react to the monthly alerts and notifications that flow through its systems. After graduating from UIUC, Jimmy intends to pursue a Master of Science in Financial Engineering or a related field (which university to be determined). Jimmy hopes to one day work as a full-time quantitative trader.

jhuan35@illinois.edu | [LinkedIn](https://www.linkedin.com/in/jimmy-huang-b4913922b/)

### Yuyang Zhao 

&nbsp;

## Project Description

This is our team's final report for our semester long project for "FIN 556 - Algorithmic Market Microstructure" taught by [Professor David Lariviere](https://davidl.web.illinois.edu/) in Fall 2022.

For this project, we trained a Long Short-Term Memory (LSTM) recurrent neural network (RNN) on historical data from Investors Exchange (IEX) to predict prices for various stocks. We used our LSTM's predictions to inform how we submitted intraday orders. We backtested our strategy on thousands of gigabytes of historical data using the RCM-X Strategy Studio software suite.

Our LSTM model was trained in Python on Google Colab. We used the Keras library to build our model, and we trained it on multidimensional price data. Our multidimensional input had 4 columns: trade price, size, bid price, and ask price. We opted for a multidimentional input over a one-dimensional input with just price because we wanted our LSTM model to incorporate more information from the order book during its training process. Ideally, the higher-dimensional data would lead to more accurate predictions. We created our multidimensional dataset by combining a dataset of trades occurring on IEX with a dataset of order book updates on IEX. We binned the data into bins of 2 second increments to allow us to merge the 2 datasets together. We structured our LSTM model such that the model looked at the previous 2 minutes of data every 2 seconds to make a prediction about the price in the next 10 minutes. We tested our model on 20% of the combined dataset and achieved a low RMSE.

After training our LSTM model in Python, we saved the model in the h5 file format. We converted this h5 file into a JSON file named fdeep_model.json using a C++ library called frugally-deep. We chose to use the frugally-deep library because it was compatible with keras-based models and did not require too much setup. In our C++ code in Strategy Studio, we maintained an input tensor (from the frugally-deep library) that represented the most recent 2 minutes of trade and order book data. Everytime the OnTrade() callback in Strategy Studio is triggered, our model (loaded in by the fdeep_model.json file) gives a prediction for the price 10 minutes from the present moment by being fed the input tensor. If the predicted price is higher than the current price, we send a buy order. If the predicted price is lower than the current price, we send a sell order. 

&nbsp;

## Technologies

(Note: The images and information about LSTM in this section are derived from [this article](https://colah.github.io/posts/2015-08-Understanding-LSTMs/))

The machine learning (ML) model that we chose for our project was Long Short-Term Memory (LSTM). LSTM is especially well-suited for the task of stock price prediction because it is a model that takes into account the contextual information of sequential data points as it trains, which is something that a vanilla multilayer perceptron does not do. Being able to learn from contextual information of nearby data points is extremely important for tasks related to Natural Language Processing (because all the surrounding words need to be taken into account in order for an ML model to understand a sentence) and time series data (which is exactly the kind of data that we dealt with in our project). LSTM is a special type of recurrent neural network (RNN). RNNs are similar to vanilla neural networks except that they have loops, which allow information to persist in the network. If we compare this to a vanilla multilayer perceptron trained on some sort of sequential data, the perceptron will make a prediction about the future data based on the current input data. After it is done making this prediction, it will be fed the next piece of input data and completely forget the previous input data it had, as well as the prediction that it made. In contrast, an RNN is able to "remember" past events by having a loop that retains information.

![rnn_loop](./images/rnn_loop.png)

In the diagram, *A* represents some amount of nodes, *x<sub>t</sub>* is some input, and *h<sub>t</sub>* is some output. If we unroll the loop, we get something like this:

![unrolled_rnn](./images/unrolled_rnn.png)

This diagram shows how RNNs are closely related to sequences and thus why they are naturally well-suited for dealing with sequential data.

There is one main problem with RNNs: although they are able to memorize past information and use that contextual information to make predictions, they are usually only good at memorizing *recent* information. RNNs have trouble memorizing long-term dependencies, which can be necessary in certains parts of a dataset where the relevant contextual information was seen a long time ago.

LSTM is a specific type of RNN that *can* learn long-term dependencies. Here is the structure of an LSTM:

![lstm_structure](./images/lstm_structure.png)

![lstm_key](./images/lstm_key.png)

The essential idea is that LSTM has the ability to optionally alter the default cell state that passes through the sequential nodes. The default cell state is unchanged by the yellow neural net layers:

![lstm_cell_state](./images/lstm_cell_state.png)

When we include the yellow neural net layers, the information passed along can be changed. Some of the key yellow layers include the forget gate layer and the input gate layer. The forget layer allows the model to retain a certain amount of information from the previous LSTM layer, and forget the rest. The input gate layer allows for new information to be stored in the cell state. Through this process of deciding what past information to memorize and what new information to incorporate, LSTM is able to learn long-term dependencies as well as short-term ones.

This was a surface-level introduction to LSTM. More detail can be found in [this article](https://colah.github.io/posts/2015-08-Understanding-LSTMs/).

&nbsp;

As for the other technologies besides LSTM that we used in our project, we chose to use the Keras library in Python due to its ease of use and high degree of functionality. (Keras is a popular open-source software library that provides a Python interface for artificial neural networks, and it acts as an interface for the TensorFlow library.)

As for other Python libraries, we used pandas for our stock data, numpy for matrix operations, matplotlib for plotting, and sklearn for data normalization (data normalization is the process of transforming your data into a range between 0 and 1). Data normalization is especially important for stock price prediction because we need our LSTM model to learn price changes during the training phase that are comparable to price changes that the model will see during backtesting. Without normalization, our model will simply learn the raw price values of the training set, which will translate into low accuracy once the model backtests on days where the overal stock price level is significantly different from that of the days trained on. 

While we trained our model entirely in Python, we wanted to make predictions with it in C++ due to the much higher exeuction speed that C++ offers. We needed a C++ library that can read in Keras models and make predictions with them when input data is fed. We went with a C++ library called [frugally-deep](https://github.com/Dobiasd/frugally-deep). Frugally-deep is a header-only library that requires minimal setup and does not require linking your application against TensorFlow. Its model.predict() function is also easy to use and is compatible with the sequential model that we made in Python. In order to use frugally-deep, we first downloaded our LSTM model locally in the .h5 file format from our code on Google Colab. We then ran frugally-deep's convert_model.py file, which converted our .h5 file into a JSON file called fdeep_model.json. We loaded this JSON file in our C++ code to create a model variable and then used the model.predict() function.

One other C++ library that we considered using was [cppflow](https://github.com/serizba/cppflow), which provides similar functionality to frugally-deep. We ran into various issues with installing cppflow on our Linux VM, so we ended up going with frugally-deep. With that said, if no installation issues are faced when using cppflow, it is a perfectly good library to use.

We used the RCM-X Strategy Studio software suite to implement our trading strategy and run backtests. Strategy Studio offers a wide and useful API to access information on the order book and send orders to exchanges. Its backtesting process also displays a helpful Profit and Loss (PnL) graph to help users evaluate their trading strategies.

Professor David Lariviere provided us a Vagrant-managed VirtualBox VM with Ubuntu 20.04 for running RCM-X's Strategy Studio. Professor Lariviere also provided us with an IEX Downloader-Parser program to be used on the VM. This Downloader-Parser automatically downloads and parses market data from IEX; it also generates the file formats expected by Strategy Studio for backtesting. 

Some of the data files downloaded through this IEX Downloader-Parser include 2 csv files called 20211105_book_updates.csv and 20211105_trades.csv. The book updates file contains timestamped data about changes to the order book on 11/05/2021 for various stocks. The first 3 price levels for the bid and ask are recorded:

![book_updates_big](./images/book_updates_big.png)

The trades file contains timestamped data about trades that are executed on an exchange for a variety of stocks. The price and size of the executed trades are recorded:

![trades_big](./images/trades_big.png)

We wanted to somehow combine these 2 csv files into 1 giant dataset for our LSTM model to train on. We wanted to combine them because we figured that incorporating more features into our model, such as trade size and bid and ask prices, would improve the accuracy of our model. The obstacle to doing this combination process is that both of these csv files have different timestamps (because book updates are completely different from trade executions, and there is no fundamental reason why the two things should occur in sync; one of the few cases when they would is if an executed trade ends up completely taking out bid or ask level(s), in which case there will clearly be an update to the order book). 
To get around this issue, we decided to bin both csv files with 2-second bins. Every row in the original csv file that is in a 2-second bin will be aggregated with the other rows in the same 2-second bin. The trade price and size values were averaged within their 2-second bins, while the minimum was taken across the BID_PRICE_1 values, and the maximum was taken across the ASK_PRICE_1 values. The BID_PRICE_1 represents the highest bid price on the order book at the specific timestamp. The ASK_PRICE_1 represents the lowest ask price on the order book at the specific timestamp. We figured that taking the minimum of BID_PRICE_1 and the maximum of ASK_PRICE_1 for the 2-second bins could potentially be more useful than taking the average of each, because we would thus be able to capture the widest bid-ask spread that existed within those 2 seconds. We thought that the widest bid-ask spread within each 2-second timeframe would be an interesting feature that our LSTM model could learn from.

Here is a toy example of how our binning process worked for the book_updates csv file. Below is the toy example before binning: (note that the timestamp in the first row means 11:27 AM at the 42nd second and zero tenths of a second)

| Timestamp    | Ticker    | BID_PRICE_1    | ASK_PRICE_1    |
| :---         |   :----:  |      :----:    |           ---: |
| 11:27:42.0   |   AAPL    |     147.66     |    153.82      |
| 11:27:42.3   |   AAPL    |     147.72     |    153.79      |
| 11:27:42.7   |   AAPL    |     147.68     |    153.81      |
| 11:27:42.9   |   AAPL    |     147.63     |    153.84      |
| 11:27:43.1   |   AAPL    |     147.67     |    153.88      |
| 11:27:43.4   |   AAPL    |     147.73     |    153.91      |
| 11:27:43.8   |   AAPL    |     147.76     |    153.92      |
| 11:27:44.1   |   AAPL    |     ...        |    ...         |

The first 7 entries in this toy dataframe occurred within the 2-second time window between 11:27:42 and 11:27:44. Thus, we aggregate them all into a single row. In the case of the book updates csv file, we take the minimum BID_PRICE_1 and maximum ASK_PRICE_1 within that 2-second window. Here is the toy example after our binning process:

| Timestamp    | Ticker    | BID_PRICE_1    | ASK_PRICE_1    |
| :---         |   :----:  |      :----:    |           ---: |
| 11:27:42     |   AAPL    |     147.63     |    153.92      |
| 11:27:44     |   AAPL    |     ...        |    ...         |

This binning process was done for all of the rows of book_updates.csv.

Here is a similar toy example, but for trades.csv and using averaging to create the bins. This is what it might look like before binning. Note that the timestamps are not the same as those in book_updates.csv, which is the whole reason why we did this binning process:

| Timestamp    | Ticker    | Trade Price    | Trade Size     |
| :---         |   :----:  |      :----:    |           ---: |
| 11:27:42.1   |   AAPL    |     151.23     |       10       |
| 11:27:42.8   |   AAPL    |     152.36     |       25       |
| 11:27:43.5   |   AAPL    |     151.56     |       20       |
| 11:27:43.9   |   AAPL    |     151.28     |       12       |
| 11:27:44.1   |   AAPL    |     ...        |    ...         |

This is what it would look like after binning:

| Timestamp    | Ticker    | Trade Price    | Trade Size     |
| :---         |   :----:  |      :----:    |           ---: |
| 11:27:42     |   AAPL    |  151.6075      |       16.75    |
| 11:27:44     |   AAPL    |     ...        |    ...         |

Notice that the binned versions of the toy book_updates example and the toy trades example both have the same timestamps. Thus, they can be combined like so:

| Timestamp    | Ticker    | Trade Price    | Trade Size     | BID_PRICE_1    | ASK_PRICE_1    |
| :---         |   :----:  |      :----:    |           ---: |      :----:    |           ---: |
| 11:27:42     |   AAPL    |  151.6075      |       16.75    |     147.63     |    153.92      |
| 11:27:44     |   AAPL    |     ...        |    ...         |     ...        |    ...         |

This was all just a toy demonstration. We can also show the exact dataset that we got we ran this binning process on our actual data. We combined the book_updates.csv and trades.csv files after binning each of them, giving us this combined_data.csv:

![combined_data_view](./images/combined_data_view.png)

With the binning and combining steps out of the way, we could now feed in these 4 columns (AVG(PRICE), AVG(SIZE), MIN(BID_PRICE_1), MAX(ASK_PRICE_1)) into our LSTM model conveniently, allowing the model to take into account more features besides just the price. 

We completed the entire binning and combination process using the R programming language. The R code for it can be found in our [data_cleaner.rmd](./data_cleaner.rmd) file.

&nbsp;

Finally, another key technology we used was GitLab for version control. We did not end up taking advantage of GitLab's DevOps and CI/CD capabilities for this project, so we just as easily could have used GitHub.

&nbsp;

## Components

![FIN_556_Components_1](./images/FIN_556_Components_1.png)

Above is a visual diagram of the different steps involved in the process we took to develop our strategy. In this section, we will also go into more detail on the C++ code we wrote to make predictions with our model and send orders. These details correspond to the green "Strategy Studio" step in the diagram.

The following code is a combination of the most relevant lines from our [LSTMstrategy.cpp](./strategy/LSTMstrategy.cpp) file. 

```ruby
#include <fdeep/fdeep.hpp>

const auto model = fdeep::load_model("fdeep_model.json");
fdeep::tensor input_tensor(fdeep::tensor_shape(60, 4), 0.0);
int row_to_fill = 0;

void LSTMStrategy::OnTrade(const TradeDataEventMsg& msg)
{
    double trade_price = msg.trade().price();
    double trade_size = msg.trade().size();
    double bid_price = msg.instrument().aggregate_order_book().BidPriceLevelAtLevel(1)->price();
    double ask_price = msg.instrument().aggregate_order_book().AskPriceLevelAtLevel(1)->price();

    if (row_to_fill < 60) {
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), ask_price);
        ++row_to_fill;
    } else {
        for (int row = 0; row < 59; ++row) {
            for (int col = 0; col < 4; ++col) {
                input_tensor.set(fdeep::tensor_pos(row, col), input_tensor.get(fdeep::tensor_pos(row + 1, col)));
            }
        }
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(59, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), ask_price);
    }

    if (row_to_fill == 60) {
        const auto result = model.predict({input_tensor});

        if (result > trade_price) {
            OrderParams params(
                msg.instrument(),
                10,
                ask_price,
                MARKET_CENTER_ID_NASDAQ,
                ORDER_SIDE_BUY,
                ORDER_TIF_DAY,
                ORDER_TYPE_LIMIT
            );
            trade_actions()->SendNewOrder(params);
        } else if (result < trade_price) {
            OrderParams params(
                msg.instrument(),
                10,
                bid_price,
                MARKET_CENTER_ID_NASDAQ,
                ORDER_SIDE_SELL,
                ORDER_TIF_DAY,
                ORDER_TYPE_LIMIT
            );
            trade_actions()->SendNewOrder(params);
        }
    }
}
```

We will explain each part of the code. Here is the first part:

```ruby
#include <fdeep/fdeep.hpp>

const auto model = fdeep::load_model("fdeep_model.json");
fdeep::tensor input_tensor(fdeep::tensor_shape(60, 4), 0.0);
int row_to_fill = 0;
```

Here we are including the frugally-deep header and creating a model variable from our fdeep_model.json file. This model variable contains information about the trained weights of our LSTM model. We also create a variable called input_tensor which is a 60 by 4 matrix. This matrix will be fed into our LSTM model to make a prediction about the future price everytime a trade occurs. The 4 columns of the matrix are exactly the 4 features we trained the model on in Python; trade price, trade size, bid price, and ask price. Here is a subset of our combined_data.csv showing these 4 columns (we also showed a similar image in the Technologies section):

![valid_snip](./images/valid_snip.png)

The first dimension of our matrix, 60, is exactly the amount of previous trades whose information we are retaining for making predictions. So, when the matrix is full, it will have 60 rows of data on the last 60 trades that occurred. The model will make predictions based on these recent trades. As for why the number 60 was chosen, it is somewhat arbitrary. We found that 60 rows was suitable, in terms of leading to a sufficient accuracy, when we were training our LSTM model in Python. It is possible that more thorough cross-validation on this value could be conducted to lead to marginally better results.

We initialize our input_tensor with zeros. It gets populated with real values when the program runs and reads in trade data. 

Finally, we have a variable called row_to_fill which is simply used as a counter to keep track of which row of our input_tensor we need to populate next.

Here is the next part of the code:

```ruby
void LSTMStrategy::OnTrade(const TradeDataEventMsg& msg)
{
    double trade_price = msg.trade().price();
    double trade_size = msg.trade().size();
    double bid_price = msg.instrument().aggregate_order_book().BidPriceLevelAtLevel(1)->price();
    double ask_price = msg.instrument().aggregate_order_book().AskPriceLevelAtLevel(1)->price();

    if (row_to_fill < 60) {
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(row_to_fill, 0), ask_price);
        ++row_to_fill;
    } else {
        for (int row = 0; row < 59; ++row) {
            for (int col = 0; col < 4; ++col) {
                input_tensor.set(fdeep::tensor_pos(row, col), input_tensor.get(fdeep::tensor_pos(row + 1, col)));
            }
        }
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), trade_size);
        input_tensor.set(fdeep::tensor_pos(59, 0), bid_price);
        input_tensor.set(fdeep::tensor_pos(59, 0), ask_price);
    }
```

We are now in the OnTrade() function, which is called by Strategy Studio everytime a trade occurs. Information about this trade is provided via the msg parameter. We first extract the trade price, trade size, bid price, and ask price from msg. Then we add these 4 values as the highest open row of our matrix. (The matrix rows are filled starting from the top row down.) If row_to_fill is less than 60, that means that our matrix has not had all of its 60 rows filled yet. In this case, we simply set values in the highest open row, whose index is tracked by row_to_fill. We then increment row_to_fill.

Once the matrix is full, it will need to erase one of its rows to make room for the current incoming trade information. We remove the oldest row, which is the one at the top of the matrix. We then shift up all of the other rows by one. This is done in the double for-loop. Once the deletion and shifting are done, we have one empty row at the bottom, which we populate with the incoming trade info. Through this process, we are able to make sure that our input matrix always contains information on the last 60 trades that occurred.

Finally, we need to make predictions with our input matrix and send orders based on them:

```ruby
if (row_to_fill == 60) {
        const auto result = model.predict({input_tensor});

        if (result > trade_price) {
            OrderParams params(
                msg.instrument(),
                10,
                ask_price,
                MARKET_CENTER_ID_NASDAQ,
                ORDER_SIDE_BUY,
                ORDER_TIF_DAY,
                ORDER_TYPE_LIMIT
            );
            trade_actions()->SendNewOrder(params);
        } else if (result < trade_price) {
            OrderParams params(
                msg.instrument(),
                10,
                bid_price,
                MARKET_CENTER_ID_NASDAQ,
                ORDER_SIDE_SELL,
                ORDER_TIF_DAY,
                ORDER_TYPE_LIMIT
            );
            trade_actions()->SendNewOrder(params);
        }
    }
}
```

We can only feed our input matrix into our model if all of the input matrix's rows are filled; we know this is true when row_to_fill equals 60. We make a prediction with our model variable given the current state of our input_tensor. If this future price prediction is greater than the current trade price, we send a buy order. Otherwise, we send a sell order. For both orders, we buy or sell 10 shares. This is arbitrary, and more thorough experimentation could have been completed to arrive at a perhaps more optimal share amount.

&nbsp;

## Git Repo Layout

```bash
.
├── frugally-deep_includes
│   ├── FunctionalPlus
│   ├── eigen
│   ├── frugally-deep
│   └── json
├── images
│   └── ...png and jpg
├── strategy
│   ├── LSTMstrategy.cpp
│   ├── LSTMstrategy.h
│   ├── Makefile
│   ├── combined_data_model
│   └── fdeep_model.json
├── FinalReport.md
├── README.md
├── Wrapped_LSTM_model_with_combined_data (2).ipynb
└── data_cleaner.rmd
```

&nbsp;

## Instructions for using the project

1. Install Virtual Box and Vagrant
2. Use Professor Lariviere's [Vagrant setup](https://gitlab.engr.illinois.edu/shared_code/strategystudioubuntu2004) and run the `go.sh` file to download IEX data 
3. On the VM, do `cd /home/vagrant/ss/sdk/RCM/StrategyStudio/`
4. Copy the `strategy` folder from our repo into the `strategies` folder on the VM
5. Install the [frugally-deep library](https://github.com/Dobiasd/frugally-deep) and place the installation in the `includes` folder on the VM


&nbsp;

## Project Results

Below is an image showing how the price predictions of our LSTM model compared to the true values in the validation set. This particular graph was generated when we trained on AAPL price data.

![lstm_more_accurate_16_epochs](./images/lstm_more_accurate_16_epochs.png)

We can also see that this model achieved a low Root Mean Square Error (RMSE) of around 0.106:

![valid_and_rmse_more_accurate_16_epochs](./images/valid_and_rmse_more_accurate_16_epochs.png)

This loss graph also shows that our model was successfully achieving lower loss over the course of 16 epochs:

![loss_graph_more_accurate_16_epochs](./images/loss_graph_more_accurate_16_epochs.png)

From these results, we would say that we have a fair amount of confidence that our LSTM model truly did learn the patterns of the training set well and generated fairly strong predictions.

&nbsp;

## Screenshots and Example Outputs

&nbsp;

## Postmortem Summary

&nbsp;

### Ayan Bhowmick

1. What did you specifically do individually for this project?
    - I lead and organized team meetings, prepared tasks for each meeting, and made sure we followed through.
    - I did some reading on LSTM to understand how it works.
    - I worked with Jimmy on the Python code to run the LSTM. I provided key insights in understanding the Python code base as well as the modifications necessary to ensure the LSTM model works with our input data. 
    - I provided the direction for what features to use. Features were decided based on research I did documenting the issues of large autocorrelation on input data.
    - I created and completed the data_cleaning.Rmd file. Essentially, I prepared all of the data used to train the LSTM, including implementing the ideas of binning the data to combine both datasets, thus providing the combined_data.csv.
    - I did research on the best optimizer to use for our model and the advantages of each optimizer. I concluded that Adam and simple SGD would be the best for our purposes, but Adam requires less tuning and runs faster. Thus, we used Adam as our optimizer.

2. What did you learn as a result of doing your project?
    I learned a lot about how AI works, how to set it up, and using the Keras library. I learned a lot about the theory behind AI in specific.

3. If you had a time machine and could go back to the beginning, what would you have done 
differently?
    I don't think I would have done anything differently. Rather, we are continuing work on this project so there is nothing I need to change that can't be changed.

4. If you were to continue working on this project, what would you continue to do to improve it, how, 
and why?
    I want to do more feature analysis and iterations over different numbers of past inputs. We currently are using 60 but that is arbitrary. I would also like to add more features to provide more information to our LSTM model - specifically collected so that there is little autocorrelation and it is only new information that is coming in.

5. What advice do you offer to future students taking this course and working on their semester long 
project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to
future students will be weighed heavily in evaluating your responses.
    I would tell students to work with Professor David to get the C++ example code working as soon as possible. This is the last and longest step for the computer to process, so having a good understanding of RCM-X is key to finishing on time.


&nbsp;

### Jimmy Huang

1. What did you specifically do individually for this project?
    - I initially proposed to our team that we should use LSTM as the backbone of our trading strategy.
    - I did some reading on LSTM to understand how it works. 
    - I got all of our Python code up and running on Google Colab. I encapsulated many parts of the code into functions to make it easier to make slight changes and compare the effects. I figured out how to change the code so that the LSTM model would train on multidimensional data as opposed to single dimensional data. I ran the entire notebook the most often on my personal laptop and generated the prediction plots.
    - I uploaded our combined_data.csv into Google Colab and trained the model on it. I downloaded the model in the h5 file format. I then followed the instructions on the frugally-deep library repo to install the library and convert the h5 file into fdeep_model.json.
    - I copied the fdeep_model.json to the VM and wrote all of the C++ code in LSTMstrategy.cpp (besides the boilerplate code provided by Strategy Studio).
    - I wrote the entirety of the following sections of this final report: Project Description, Technologies, Components, Git Repo Layout, Instructions, and Project Results.
2. What did you learn as a result of doing your project?
    - I learned a lot about the general theory behind LSTM through my reading. One aspect that I remember in particular from using the Python code for LSTM is that LSTM uses a lookback period which defines how many previous timesteps are used to predict the subsequent timestep. Before looking at the code, I had the preconceived notion that an explicit value such as the lookback period would not be something that would need to be defined, as I figured that LSTM would entirely figure out on its own how many previous timesteps to use, and that this amount would change dynamically. Learning about the lookback period was important since it made me realize that there are many aspects of the LSTM model that are arbitrary and that can be cross-validated to be optimized.
    - The Strategy Studio software suite was completely new to me this semester. I learned about the most important syntax for using the major functions in Strategy Studio's Development API, such as how the OnTrade() function works as well as how to access trade information like price, size, bid, and ask.
    - I learned that having a simple Python model that seems accurate when training on price data is really just a miniscule step in the grand process of creating a truly profitable trading strategy. There are many nitty-gritty details that need to be considered when executing a trained model in C++; I did not foresee many of these details before taking this class. For example, if our model predicts that the stock price is going to increase, how many shares should we buy? Which stock exchange should we route our order to? Could we gain more profit by purchasing a call option instead of the underlying? What if we buy both? How long should we hold the assets we purchase? Such questions could perhaps by answered by simply trying everything during many backtests, but I imagine that complicated algorithms are used in the real industry to make decisions on these sorts of things. I only became aware of such details as a result of working on this project. 
    - I learned about how to take a model trained in Python and make predictions with it in C++. At the start of this semester, I was initially under the impression that we would have to duplicate the entirety of our Python notebook in C++ in order to use our model in Strategy Studio. I learned that it's much more convenient to save the Python model as a local file and then load that file into your C++ code using a C++ library such as frugally-deep.
3. If you had a time machine and could go back to the beginning, what would you have done 
differently?
    - I would have spent more time on the implementation of our strategy in Strategy Studio, and less time on tweaking our model in Python. 
    - I would have put a lot more thought into writing a detailed project proposal. There are many details that I did not foresee when working on our initial project proposal. 
    - I would have assigned much more specific tasks to our team, and much more frequently. The issues we assigned in GitLab this semester were a bit too vague.
    - I would have spent more time in the beginning of the semester getting to know the other people in the class so that I could have a better time picking a group that I would work well in.
4. If you were to continue working on this project, what would you continue to do to improve it, how, 
and why?
    - Run more backtests. We could vary the length of backtest periods and also pick specific historical time periods to experiment on. 
    - Engage in a more thorough cross-validation process for various hard-coded values. Some examples of hard-coded values are the lookback period for the LSTM model as well as the share amount we bought or sold in the OnTrade() function.
    - Train a more complicated LSTM model in Python. We could incorporate even more input columns, such as the second and third bid and ask price levels. 
    - Try additional strategies and compare them. We could compare our LSTM strategy to non-ML ones like momentum and mean-reversion. We could also encapsulate all these strategies in Strategy Studio in such a way that they could all be used simultaneously.
    - We could create a bash script that condenses all of the instructions for our project into fewer steps.
    - We could train more LSTM models on different assets. We mainly tested with AAPL, but we should try ETFs like SPY and QQQ.
    - We could do a lot more investigation into the functionality of the Strategy Studio Development API, and we could make our C++ code more complex. For example, we could incorporate some logic to determine the best exchange to route an order to based on some criteria, instead of just sending to an arbitrary exchange. 
5. What advice do you offer to future students taking this course and working on their semester long 
project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to
future students will be weighed heavily in evaluating your responses.
    - My number one piece of advice would be to make sure that your first priority for the project is getting a complete strategy to work from start to end. What I mean is that, if we look at the Components section of this report, you should try to get the entire process functioning as quickly as possible, regardless if the quality of each component is pretty bad. It's better to have all the components working correctly with each component being fairly mediocre at first, than having only a few components working and not being able to run the whole process from start to finish. I recommend just completing a minimum viable product version of each component and making sure that they can all work together. Then you can go back and make everything better. This ensures that you have some peace of mind when working on the project, since from early on you'll know for sure that at least you can run everything. Also, it makes more sense to do it this way because at some point you can't even tell if an earlier component is good enough until you've actually seen the results of a backtest. I wish that my group had followed this piece of advice; we mainly focused on the earlier components too much for the first half of the semester, so we had to scramble a bit to actually get to the Strategy Studio and PnL components. 
    - Make a very detailed project proposal, and do a lot of research to figure out all the nitty-gritty tasks that you will probably need to accomplish in order to complete each step of the project. Our group's project proposal was pretty much something along the lines of, "Make LSTM model in Python. Then transfer to C++. Finally, backtest." While technically correct, such a series of steps was way too vague to be useful to us. When I actually got to working through all the different parts of the project, there were so many small things that I didn't ever think about that made our workflow inefficient. For example, I somewhat naively thought at the beginning of the semester that the way we would transfer our model to Strategy Studio was by reimplementing the entirety of our Python code in C++. Only around a bit after halfway through the semester did we realize that all we had to do was save the Python model to a local file and then load that in our cpp file. If we had just taken a little more time to research ways to do machine learning in C++, we probably would have foreseen this. Of course, in general, it is hard to foresee every single little detail that you will end up having to figure out when you actually get to working on the project, especially if this class is the first time that you've worked on such a project. However, you should still at least put in a good amount of time in the research and project proposal phase and do your due diligence.
    - Do not underestimate the amount of lower level programming and general setup that is required for Strategy Studio to work. As stated before, my group spent too much time with the Python aspect and had to rush a bit to get the Strategy Studio components working. There are lots of possibly annoying steps to go through, such as downloading Cygwin (if you use Windows) to make using the VM easier, getting your ssh keys set up for GitLab, modifying the list of hostnames that VSCode recognizes, changing PATH variables, and editing Makefiles. I personally was not mentally prepared for all of these kinds of steps at the beginning of this semester. This is another reason why my first piece of advice is useful; you want to make sure that you get these nitty-gritty setup steps out of the way early on.
    - Ask a LOT of questions. The other students in the class, as well as the professor, are great resources for overcoming obstacles in the project and gaining new ideas. In this semester in particular, a lot of the other student groups were using machine learning algorithms, so there was quite a bit of similar work that we were all doing. Thus, this made it so that we could all give specific advice to each other, but only if we bothered to ask in the first place. I personally regret not asking enough questions. I think it's important to not take the time spent on doing practical projects in college for granted. The time we have in college to work on practical projects is an incredibly nice way to ease yourself into the kind of work that might be done in industry, because no one in college really cares if you have no idea what you're doing, or if your results are mediocre. Our professor this semester specifically mentioned that he doesn't grade projects based on how good their Profit and Loss results are. The professor does not care too much if your trading strategy reports a profit of negative $1 million. The professor mainly cares that the students engage in the full strategy creation process and try a lot of things. This class is a great opportunity to fail fast and often without much repercussion. I imagine that the real-world industry is much less forgiving.

&nbsp;

### Yuyang Zhao

1. What did you specifically do individually for this project?
2. What did you learn as a result of doing your project?
3. If you had a time machine and could go back to the beginning, what would you have done 
differently?
4. If you were to continue working on this project, what would you continue to do to improve it, how, 
and why?
5. What advice do you offer to future students taking this course and working on their semester long 
project (be sides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to
future students will be weighed heavily in evaluating your responses.





