# LSTM trading strategy


## Description
An LSTM trading strategy trained in Python and exectued in C++ in Strategy Studio. The final report for this project is [here](./FinalReport.md).


## License
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

